import { BehaviorSubject } from 'rxjs';
import type { Person } from './person.model';

export class PersonsService {
	public persons$ = new BehaviorSubject<Array<Person>>([
		{
			id: 0,
			fullName: 'Вася'
		},
		{
			id: 1,
			fullName: 'Гурбангулы Бердымухамедов'
		},
	]);

	public delete(id: number): void {
		const existing = [...this.persons$.getValue()];
		this.persons$.next(existing.filter(person => person.id !== id));
	}
}

const personsService = new PersonsService();

export { personsService };
